using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TYfinale : MonoBehaviour
{
   

    void OnTriggerExit()
    {
        StartCoroutine(byebye());
    }

    IEnumerator byebye()
    {
        yield return new WaitForSeconds(10f);
        SceneManager.LoadScene("MainMenu");
    }
}
