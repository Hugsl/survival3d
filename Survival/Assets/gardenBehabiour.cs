using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gardenBehabiour : MonoBehaviour
{
    public static event Action<bool> gT = delegate { };

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            gT.Invoke(true);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            gT.Invoke(false);
        }
    }
}
