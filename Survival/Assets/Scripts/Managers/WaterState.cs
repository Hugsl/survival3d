using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterState : MonoBehaviour,IDataPersistence
{
   [SerializeField] private int _lvLWater;
    private GameObject _water;
    public delegate void AltarsActivation(int waterLvl);
    public static event AltarsActivation AA;


    private void OnEnable()
    {
        _water = GameObject.Find("WaterPlane");
        _water.GetComponent<MeshCollider>().enabled = false;
        _lvLWater = 0;
        StoneBehabiour.WaterActivation += waterUp;
    }
    private void OnDisable()
    {
        StoneBehabiour.WaterActivation -= waterUp;

    }
    private void waterUp()
    {
        _lvLWater++;
        _water.GetComponent<Animator>().SetInteger("WaterLvl",_lvLWater);
        AA.Invoke(_lvLWater);
        if (_lvLWater >= 2) _water.GetComponent<MeshCollider>().enabled = true;
    }
    private void waterUp(bool booted) {

        _water.GetComponent<Animator>().SetInteger("WaterLvl", _lvLWater);
        AA.Invoke(_lvLWater);
        if (_lvLWater >= 2) _water.GetComponent<MeshCollider>().enabled = true;

    }
    public void endLevel() {

        waterUp();
    }
    public void LoadData(GameData data)
    {
       _lvLWater=data.waterLevel;
        waterUp(true);
        Debug.Log(data.waterLevel + " = nivel del agua cargado");
    }

    public void SaveData(ref GameData data)
    {
       data.waterLevel = _lvLWater;
       Debug.Log(data.waterLevel + " = nivel del agua guardado");

    }
}
