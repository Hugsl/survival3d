using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private PlayerControl playerControls;
    private static InputManager _instance;



    //Triggers

    public static InputManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else _instance = this;
        playerControls = new PlayerControl();
    }
    private void OnEnable()
    {
        playerControls.Enable();
    }
    private void OnDisable()
    {
        playerControls.Disable();
    }
    public Vector2 GetPlayerMovement()
    {
        return playerControls.PlayerControls.Movement.ReadValue<Vector2>();
    }

    public Vector2 GetMouseDelta()
    {
        return playerControls.PlayerControls.Camera.ReadValue<Vector2>();
    }

   public bool PlayerSprintedThisFrame()
    {
        return playerControls.PlayerControls.Sprint.IsPressed();
    }
    public bool PlayerCrouchedThisFrame()
    {
        return playerControls.PlayerControls.Crouch.IsPressed();
    }
    public bool PlayerJumpedThisFrame()
    {
        return playerControls.PlayerControls.Jump.IsPressed();
    }
    public bool PlayerDancedThisFrame()
    {
        return playerControls.PlayerControls.GoofyAhhh.IsPressed();
    }
    public bool PlayerAttackedThisFrame()
    {
        return playerControls.PlayerControls.AttackShoot.IsPressed();
        
    }
    public bool PlayerAimThisFrame()
    {
        return playerControls.PlayerControls.BlockAim.IsPressed();
    }

    public bool PlayerInteractedThisFrame()
    {
        return playerControls.PlayerControls.Interact.IsInProgress();
    }
}
