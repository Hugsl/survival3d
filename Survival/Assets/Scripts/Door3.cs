using SunTemple;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door3 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {


             GetComponent<Door>().Open();
        

        }
    }
    private void OnTriggerExit(Collider other)
    {
        GetComponent<Door>().Close();
    }
}
