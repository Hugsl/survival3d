using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPortal : MonoBehaviour, IDataPersistence
{

    public static event Action FlashBang = delegate { };
    private void OnEnable()
    {
        WaterState.AA += lightsUp;
    }
    private void OnDisable()
    {
        WaterState.AA -= lightsUp;
    }

    private void lightsUp(int waterLevel)
    {
        var Effects = GetComponent<ParticleSystem>();
        if (waterLevel >= 4)
        {
            Effects.Play();
            GetComponent<Collider>().enabled = true;
        }
        else { Effects.Stop(); GetComponent<Collider>().enabled = false; }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag=="Player")
        {


            GameObject.Find("DataPersistenceManager").GetComponent<DataPersistanceManager>().resetGame();
            FlashBang.Invoke();

        }
    }

    public void LoadData(GameData data)
    {
        lightsUp(data.waterLevel);
    }

    public void SaveData(ref GameData data)
    {
 
    //Nothing to save, yet
    }
}
