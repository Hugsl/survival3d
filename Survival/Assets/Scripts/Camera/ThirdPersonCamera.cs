using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{

    [Header("References")]
    public Transform orientation;
    public Transform player;
    public Transform playerObj;
    public CharacterController cc;
    public InputManager IM;

    public float rotationSpeed;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        
        // para la orientacion de rotaci�n
        Vector3 viewDir = player.position - new Vector3(transform.position.x, transform.position.y, transform.position.z);
        orientation.forward = viewDir.normalized;
        
        //Rotar el objeto de personaje
        Vector2 movementInput = IM.GetPlayerMovement();
        Vector3 inputDir = orientation.forward * movementInput.y + orientation.right * movementInput.x;
        inputDir = new Vector3(inputDir.x,0,inputDir.z);

        if (inputDir != Vector3.zero)
            playerObj.forward = Vector3.Slerp(playerObj.forward,inputDir.normalized,Time.deltaTime*rotationSpeed);
    
    }
}
