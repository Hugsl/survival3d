using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    public static void startGame()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public static void returnToMenu()
    {
        SceneManager.LoadScene("TYScreen");
    }
    public static void exitGame()
    {
        Application.Quit();
    }
}
