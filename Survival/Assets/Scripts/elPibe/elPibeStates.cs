using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elPibeStates : MonoBehaviour
{

    [SerializeField] elPibeNavigation patrol;
    [SerializeField] elPibeSeguir follow;
    [SerializeField] elPibeVision fov;


    [SerializeField]
    public elPibeState eState;
    private void OnEnable()
    {
        gardenBehabiour.gT += changeITG;
        elPibeVision.spoted += changetoFollow;
    }

    private void OnDisable()
    {
        gardenBehabiour.gT -= changeITG;
        elPibeVision.spoted -= changetoFollow;
    }
    public enum elPibeState
    {
        Patrolling,
        Follow,
      Fov       
    }
    
    public void ChangeState(elPibeState state)
    {
        eState = state;

        DisableBehaviours();

        switch (eState)
        {
            case elPibeState.Patrolling:
                patrol.enabled = true
                    ;
             
                break;
            case elPibeState.Follow:
                follow.enabled = true;
               
                break;
            case elPibeState.Fov: fov.enabled = true; patrol.enabled = true;
                break;
            default:
                break;
        }
    }

    void DisableBehaviours()
    {
        patrol.enabled = false;
        follow.enabled = false;
    
    }

    private void changeITG(bool active)
    { DisableBehaviours();
        if (active){
            eState = elPibeState.Fov; }
        else { 
            eState = elPibeState.Patrolling; }
     
        ChangeState(eState);
    }

    void changetoFollow()
    {
        eState = elPibeState.Follow;
        ChangeState (eState);

    }

}
