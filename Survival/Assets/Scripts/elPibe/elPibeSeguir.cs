using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class elPibeSeguir : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] Transform follow;
    [SerializeField] LayerMask obstacleMask, playerMask;
    [SerializeField] GameObject elpibeCanvas;

    [SerializeField] float newRadious, newAngle;



    

    elPibeStates enemyStates;
    private void OnEnable()
    {
        if (follow == null) follow = GameObject.Find("Player").GetComponent<Transform>();
        GetComponent<Animator>().SetInteger("State", 2);
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 2;


  



        enemyStates = GetComponent<elPibeStates>();

    }

    private void OnDisable()
    {
            GetComponent<elPibeNavigation>().UpdateDestination();
    }
    private void Start()
    {
        elpibeCanvas = GameObject.Find("elPibeCanvas");
        elpibeCanvas.SetActive(false);  
    }

    public void Update()
    {
                
       

            Follow();

        
    }

    public void Follow()
    {
        if (Vector3.Distance(this.transform.position, follow.position) > 2.00f)
        {
            agent.destination = follow.position;
            GetComponent<Animator>().SetInteger("State", 3);
            elpibeCanvas.SetActive(false);
        }
        else
        {
            GetComponent<Animator>().SetInteger("State", 4);
            elpibeCanvas.SetActive(true);
        }

    }

   

   

 
    private void OnTriggerEnter(Collider other)
    {
      //  pray = true;
       // GetComponent<Animator>().SetInteger("State", 4);
        
    }

    private void OnTriggerExit(Collider other)
    {
      //  pray = false;
       // GetComponent<Animator>().SetInteger("State", 3);
       // GetComponent<elPibeNavigation>().GetCloserPatrolPoint();
    }

}
