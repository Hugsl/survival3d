using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elPibeVision : MonoBehaviour
{
    private void OnEnable()
    {
        gardenBehabiour.gT += changeBool;
    }
    private void OnDisable()
    {
        gardenBehabiour.gT -= changeBool;
    }


    public static event Action spoted = delegate { };




    public bool isInsideGarden;




    void changeBool(bool a)
    {
        isInsideGarden = a;
    }



    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player" && isInsideGarden)
        {
            GetComponent<Animator>().SetInteger("State", 3);
            spoted.Invoke();
        }
    }
    private void OnTriggerExit(Collider other)
    {
    
    }



}
