using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class elPibeNavigation : MonoBehaviour
{
    [SerializeField] List<Transform> PoI;
    [SerializeField] int currentPoint;
    bool wait;
    bool backwards;
    bool hasreachedtheend;
    bool turnBackToOnePoint;
    Vector3 target;
    NavMeshAgent agent;
    Animator animator;
    private Coroutine cor = null;
    // Start is called before the first frame update
    
    
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("State",1);
        wait = false;
        backwards = true; 
        hasreachedtheend = true; 
        foreach (var l in GameObject.FindGameObjectsWithTag("FollowMe"))
        {

            PoI.Add(l.transform);
          
        }
        target = PoI[0].position;
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 0.75f;
        GetCloserPatrolPoint();

    }

    // Update is called once per frame
    void Update()
    {
        if (!wait)
        {

          // Debug.Log(Vector3.Distance(transform.position, target));
            if (Vector3.Distance(transform.position, target) <= 0.35f && cor == null)
            {
           
                
               cor= StartCoroutine(waitAndContinuePatroling());
            }
        
        }

    }

    IEnumerator waitAndContinuePatroling()
    {
        ChangeDestinationIndex();
        animator.SetInteger("State", 0);
        yield return new WaitForSeconds(4f);
        animator.SetInteger("State", 1);
        UpdateDestination();
      

        wait = false;
        cor = null;
    }


    public void UpdateDestination()
    {
        target = PoI[currentPoint].position;
        agent.SetDestination(target);
    }

    void ChangeDestinationIndex()
    {
        if (currentPoint == PoI.Count - 1 && !hasreachedtheend) { backwards = true; hasreachedtheend = !hasreachedtheend; }
        else if (currentPoint == 0 && hasreachedtheend) {backwards = false; hasreachedtheend = !hasreachedtheend; }
        if (!backwards)currentPoint++;
        if (backwards) currentPoint--;

    }

    public void GetCloserPatrolPoint()
    {

        Transform closerPoint;

        closerPoint = PoI[0];
        foreach (var point in PoI)
            if (Vector3.Distance(transform.position, point.transform.position)
                < Vector3.Distance(transform.position, closerPoint.transform.position))
            {
                closerPoint = point;
            }

        target = closerPoint.transform.position;
        for (int i = 0; i < PoI.Count; i++)
        {
            if (PoI[i] == closerPoint)
            {
                currentPoint = i;
                break;
            }
        }


    }
}

