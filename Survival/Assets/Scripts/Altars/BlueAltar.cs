using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueAltar : MonoBehaviour
{

    [SerializeField] private int _waterLvlToReact;
    public delegate void DoorsActivation(int waterLvl);
    public static event DoorsActivation DA;


    private void OnEnable()
    {
        WaterState.AA += lightsUp;
    }

    private void OnDisable()
    {
        WaterState.AA -= lightsUp;
    }


    private void lightsUp(int waterLevel)
    {
        var Effects = GetComponentInChildren<ParticleSystem>();
        if (waterLevel >= _waterLvlToReact)
        {
            Effects.Play();
            DA.Invoke(waterLevel);
        }
        else { Effects.Stop(); }
    }

    /*
    public void LoadData(GameData data)
    {
        var Effects = GetComponentInChildren<ParticleSystem>();
        if (data.waterLevel>=_waterLvlToReact)
        {
            Effects.IsAlive(true);
            da.Invoke(data.waterLevel);
        }
        else { Effects.IsAlive(false); }
    }

    public void SaveData(ref GameData data)
    {
        //nothing to implement (yet)
    }

 */
}
