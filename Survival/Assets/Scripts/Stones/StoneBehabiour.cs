using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneBehabiour : MonoBehaviour, IDataPersistence
{


    public static event Action WaterActivation = delegate { };



    [SerializeField]
    string id;
    [ContextMenu("Generate guid for id")]
    private void GenerateGuid()
    {
        id = System.Guid.NewGuid().ToString();
    }
    [SerializeField]
    bool activated;



    private void changeStoneStatus()
    {
        if (!activated)
        {
            activated = true;
            WaterActivation.Invoke();
        }
    }



    public void LoadData(GameData data)
    {

       data.ActivatedStones.TryGetValue(id, out activated);
    
        if (activated)
        {
            //this.gameObject.SetActive(activated);


        }
    }

    public void SaveData(ref GameData data)
    {
        if (data.ActivatedStones.ContainsKey(id))
        {
            data.ActivatedStones.Remove(id);
        }
        data.ActivatedStones.Add(id, activated);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerControler.Interact += changeStoneStatus;
            Debug.Log("I'm in");
        }   
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerControler.Interact -= changeStoneStatus;
            Debug.Log("I'm out");

        }
    }
}
