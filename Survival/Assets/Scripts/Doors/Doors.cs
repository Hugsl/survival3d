using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doors : MonoBehaviour
{
    [SerializeField] private int _waterLvlToReact;
    [SerializeField] private Animator animator;
    // Start is called before the first frame update
    private void OnEnable()
    {
        BlueAltar.DA += moveDoor;
    }
    private void OnDisable()
    {
        BlueAltar.DA -= moveDoor;

    }

    private void moveDoor(int waterLevel)
    {
        if (waterLevel>= _waterLvlToReact)
        {   
            GetComponent<Animator>().SetBool("Low",true);
        }
    }
}
