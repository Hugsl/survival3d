using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashBangBehabiour : MonoBehaviour
{
    private void OnEnable()
    {
        ExitPortal.FlashBang += startAnimation;
    }
    private void OnDisable()
    {
        ExitPortal.FlashBang -= startAnimation;

    }

    private void startAnimation()
    {

        GetComponent<Animator>().SetBool("Start",true);
    }
}
