using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameData
{
    public int waterLevel;

    public bool bootsEquipped;

    public Vector3 playerPosition;
    public SerializableDictionary<string, bool> ActivatedStones;

    public bool initialDialogueDone;

    public GameData()
    {
        playerPosition =new Vector3(239.403f, 14.6199999f, 285.576996f);
        waterLevel = 0;
        bootsEquipped = false;
        ActivatedStones = new  SerializableDictionary<string, bool>();
        initialDialogueDone = false;
}
}
