using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class FileDataHandler 
{
    private string dataDirPath = "";

    private string dataFileName = "";

    private bool useEncription;
    private readonly string encriptionCoreWord = "hugo";

    public FileDataHandler(string dataDirPath,string dataFileName, bool useEncription)
    {
        this.dataDirPath = dataDirPath;
        this.dataFileName = dataFileName;
        this.useEncription = useEncription;
    }

    public GameData Load()
    {
        string fullPath = Path.Combine(dataDirPath, dataFileName);
        GameData loadedData = null;
        if (File.Exists(fullPath))
        {
            try
            {
                string dataToLoad = "";
                using (FileStream stream = new FileStream(fullPath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        dataToLoad = reader.ReadToEnd();
                    }
                }
                if (useEncription)
                {
                    dataToLoad = EncriptDecript(dataToLoad);
                }

                loadedData = JsonUtility.FromJson<GameData>(dataToLoad);
            }
            catch(Exception e)
            {
                Debug.LogError("Error with the path: " + fullPath + "\n" + e);
            }
        }
        return loadedData;
    }

    public void Save(GameData data)
    {
        string fullPath = Path.Combine(dataDirPath,dataFileName);
        try
        {
            Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
            string dataToStore = JsonUtility.ToJson(data, true);

            if (useEncription)
            {
                dataToStore = EncriptDecript(dataToStore);
            }

            using (FileStream stream = new FileStream(fullPath, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(dataToStore);
                }
            }

        }catch(Exception e)
        {
            Debug.LogError("Error Occured with this path: " + fullPath);
        }
    }

    private string EncriptDecript(string data)
    {
        string modifieddata = "";
        for (int i =0;i<data.Length;i++) {

            modifieddata += (char)(data[i] ^ encriptionCoreWord[i % encriptionCoreWord.Length]);
        
        }

        return modifieddata;
    }

}
