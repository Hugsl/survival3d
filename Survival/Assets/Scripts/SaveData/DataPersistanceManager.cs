using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


// El videito del tutorial https://youtu.be/aUi9aijvpgs


public class DataPersistanceManager : MonoBehaviour
{
    private GameData _gameData;
    private List<IDataPersistence> dataPersostences;

    public static DataPersistanceManager Instance { get; private set; }


    //Save thinguies

    [SerializeField] private string FileName;
    [SerializeField] private bool encrypt;
    private FileDataHandler _fileDataHandler;


    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Hay mas de un manager de persistencia de datos mi rey");
        }
        Instance = this; 
    }
    private void Start()
    {
        this._fileDataHandler = new FileDataHandler(Application.persistentDataPath, FileName,encrypt);
        this.dataPersostences = new List<IDataPersistence>();
        LoadGame();
    }
    public void NewGame()
    {
        this._gameData = new GameData();
    }
    public void LoadGame()
    {
        this._gameData = _fileDataHandler.Load();
      
        if (this._gameData == null ||this._gameData.waterLevel>=5)
        {
            Debug.Log("No hay datos guardados, Inicializando datos default. ");
            NewGame();
        }
   
        foreach(IDataPersistence dpo in FindAllPersistence())
        {
            dpo.LoadData(_gameData);
        }
    }
    public void SaveGame()
    {
        foreach (IDataPersistence dpo in FindAllPersistence())
        {
            dpo.SaveData(ref _gameData);
        }
        _fileDataHandler.Save(_gameData);
    }

    public void resetGame() {

        _fileDataHandler.Save(new GameData());

    }

    private void OnApplicationQuit()
    {
        SaveGame();
    }


private List<IDataPersistence> FindAllPersistence()
    {
        IEnumerable<IDataPersistence> dataPersistences = FindObjectsOfType<MonoBehaviour>().OfType<IDataPersistence>();
        return new List<IDataPersistence>(dataPersistences);
    }


}
