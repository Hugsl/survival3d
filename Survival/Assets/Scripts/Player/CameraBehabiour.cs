using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehabiour : MonoBehaviour
{
  public  GameObject Camera;
    public GameObject AimCamera;
    public InputManager InputManager;
    void Start()
    {
        InputManager = InputManager.Instance;
    }

    void Update()
    {
        if (InputManager.PlayerAimThisFrame())
        {
            Camera.SetActive(false);
            AimCamera.SetActive(true);
        }
        else
        {
            Camera.SetActive(true);
            AimCamera.SetActive(false);
        }
    }
}
