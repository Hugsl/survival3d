using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerAnimationControler : MonoBehaviour
{

    public PlayerControler player;
    public Animator animator;
    private float movementFloat;
    private float speedanimationtransition;


    private void OnEnable()
    {
        PlayerControler.DanceTrigger += danceAnimation;   
    }
    private void OnDisable()
    {
        PlayerControler.DanceTrigger -= danceAnimation;
    }

    void Start()
    {
        movementFloat = 0f;
        speedanimationtransition = 1f*Time.deltaTime;
        animator = GetComponent<Animator>();
        animator.SetFloat("MovementTree", 0);
        animator.SetBool("Jump", false);
        animator.SetBool("Crouch", false);
        animator.SetBool("GoofyAhh", false);
    }
    void Update()
    {
        if (!player.GetGroundedStatus()) movementAnimation();
        
    }
   
    
    private void movementAnimation()
    {
        switch (player.GetMovestatus())
        {
            case PlayerControler.movementStatus.idle:
                if (movementFloat > 0) movementFloat -= speedanimationtransition;
                else movementFloat = 0;
                break;
            case PlayerControler.movementStatus.walk:
                if (movementFloat > 0.5) movementFloat -= speedanimationtransition;
                else if (movementFloat < 0.5) movementFloat += speedanimationtransition;
                else movementFloat = 0.5f;
                break;
            case PlayerControler.movementStatus.run:
                if (movementFloat < 1) movementFloat += speedanimationtransition;
                else movementFloat = 1;
                break;
            case PlayerControler.movementStatus.crouch:
                crouchStatus();
                break;
        }
        
        animator.SetFloat("MovementTree", movementFloat);
    }
    private void crouchStatus()
    {
        if (!animator.GetBool("Crouch")) animator.SetBool("Crouch", true);
        else animator.SetBool("Crouch", false);
    }
    private void danceAnimation()
    {
        animator.SetBool("GoofyAhh", true);
    }
}
