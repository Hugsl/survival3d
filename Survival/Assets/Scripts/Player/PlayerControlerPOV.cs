using UnityEngine;
using UnityEngine.InputSystem;


    [RequireComponent(typeof(CharacterController))]
    public class PlayerControlerPOV : MonoBehaviour
    {
        [SerializeField] private CharacterController controller;
        [SerializeField] private Vector3 playerVelocity;
        [SerializeField] private bool groundedPlayer;
        [SerializeField] private float playerSpeed = 2.0f;
        [SerializeField] private float jumpHeight = 1.0f;
        [SerializeField] private float gravityValue = -9.81f;
        [SerializeField] private Transform cameraTransform;
        [SerializeField] private InputManager _im;
         


        private void Start()
        {
   
            controller = gameObject.GetComponent<CharacterController>();
        }

        void Update()
        {
      
            cameraRoation();

            groundedPlayer = controller.isGrounded;
            if (groundedPlayer && playerVelocity.y < 0)
            {
                playerVelocity.y = 0f;
            }


            var movement = _im.GetPlayerMovement();

            Vector3 move = new Vector3(movement.x, 0, movement.y);

            move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
            controller.Move(move * Time.deltaTime * playerSpeed);




            // Changes the height position of the player..
            if (Input.GetButtonDown("Jump") && groundedPlayer)
            {
                playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }

            playerVelocity.y += gravityValue * Time.deltaTime;
            controller.Move(playerVelocity * Time.deltaTime);

        }


    void cameraRoation()
    {
        var a = cameraTransform.rotation;
        a.x = 0;
        transform.rotation = a;
    }

}

    
  
    