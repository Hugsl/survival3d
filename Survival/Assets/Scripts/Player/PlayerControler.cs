using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class PlayerControler : MonoBehaviour, IDataPersistence
{
    
    private CharacterController controller;
    private InputManager inputManager;
    [SerializeField]
    private PlayerControl _pc;
    private UnityEngine.InputSystem.InputAction move;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    [SerializeField]
    private float playerSpeed = 2.0f;
    [SerializeField]
    private float jumpHeight = 1.0f;
    [SerializeField]
    private float sprintSpeed = 1.75f;
    private float crouchSpeed = 0.75f;
    private float gravityValue = -9.81f;

    //Triggers
    public static event Action DanceTrigger = delegate { };
    public static event Action Interact = delegate { };

    public enum movementStatus
    {
        idle,walk, run, crouch, swim, dance
    }

    public movementStatus movestatus;
    public Transform cameraTransform;
    /*private void OnEnable()
    {
      _pc = GetComponent<PlayerInput>().GetComponent<PlayerControl>();
       move = _pc.FindAction("Movement");
            move.performed += cxt => movement();
       
        var attack = _pc.FindAction("Attack/Shoot");
        attack.performed += cxt => Attack();
    }

    private void OnDisable()
    {
          move.performed -= cxt => movement();
    }
    */


    private void Start()
    {
        movestatus = movementStatus.idle;
        controller = GetComponent<CharacterController>();
        inputManager = InputManager.Instance;
        cameraTransform = Camera.main.transform;
    }

    void Update()
    {

        if (movestatus!=movementStatus.dance)
        {
            if (inputManager.PlayerInteractedThisFrame())
            {
                Interact.Invoke();
            }
            movement();
            // Changes the height position of the player..
            if (inputManager.PlayerJumpedThisFrame() && groundedPlayer)
            {

                groundedPlayer = false;
                playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }
            else
            {
                playerVelocity.y += gravityValue * Time.deltaTime;

            }

            if (inputManager.PlayerDancedThisFrame())
            {
                movestatus=movementStatus.dance;
                DanceTrigger.Invoke();
            }
            
        }

      
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }


    public void movement()
    {
        groundedPlayer = controller.isGrounded;

        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        //Lee el input del mando/teclado para moverse
        Vector2 movement = inputManager.GetPlayerMovement();
        if (movement != new Vector2(0, 0) && movestatus!=movementStatus.run) { movestatus = movementStatus.walk; }
        else if (movement == new Vector2(0, 0)) { movestatus = movementStatus.idle; }

        //comprueva si ha pulsado el botn de sprint
        if (inputManager.PlayerSprintedThisFrame())
        {
         if (movestatus == movementStatus.run)  movestatus = movementStatus.walk;
         else movestatus = movementStatus.run;
        }
        else if (inputManager.PlayerCrouchedThisFrame())
        {
            if (movestatus == movementStatus.crouch) movestatus = movementStatus.walk;
            else movestatus = movementStatus.crouch;
        }
        //status run checker
        if (movestatus == movementStatus.run)
        {
                movement.x *= sprintSpeed;
                movement.y *= sprintSpeed;
        }
        else if (movestatus == movementStatus.crouch)
        {
            movement.x *= crouchSpeed;
            movement.y *= crouchSpeed;
        }
     

        

        Vector3 move = new Vector3(movement.x, 0f, movement.y);

        move = cameraTransform.forward * move.z + cameraTransform.right * move.x;
        move.y = 0f;
        controller.Move(move * Time.deltaTime * playerSpeed);

        controller.Move(playerVelocity * Time.deltaTime);


    }
    

    public movementStatus GetMovestatus() => movestatus;
    public bool GetGroundedStatus() =>groundedPlayer;

    public void LoadData(GameData data)
    {
        Debug.Log(data.playerPosition);
        this.gameObject.transform.SetPositionAndRotation(data.playerPosition,new Quaternion());
    }

    public void SaveData(ref GameData data)
    {
        Debug.Log(this.gameObject.transform.position);
        data.playerPosition = this.gameObject.transform.position;
    }
}
