using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PlayerControlerPOV;
using UnityEngine.InputSystem.XR;

public class AtackScript : MonoBehaviour
{
    InputManager inputManager;
    PlayerControler _pc;
    [SerializeField]
    GameObject _bullet;

    private void Start()
    {
        _pc = GetComponent<PlayerControler>();
        inputManager = InputManager.Instance;
    }
    private void Update()
    {
        if (inputManager.PlayerAttackedThisFrame()) shoot();  
    }

    void shoot()
    {
        if (_pc.movestatus != PlayerControler.movementStatus.crouch || _pc.movestatus != PlayerControler.movementStatus.dance) {

            Debug.Log("I'm In");
            Instantiate(_bullet,GameObject.Find("Player").transform.position, GameObject.Find("PlayerCam").transform.rotation); 
        }
    }
}
